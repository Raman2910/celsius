package sheridan;

public class Celsius {

	public static int fromFahrenheit(int x) {
		return  Math.round((x-32)*5/9);
	}
}
